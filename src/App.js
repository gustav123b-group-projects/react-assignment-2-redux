import React, { useState, useEffect, createContext } from 'react';
import './App.css';
import Navbar from "./Components/Navbar/Navbar"
import Pages from './Components/Pages/Pages';
import { BrowserRouter } from 'react-router-dom';

export const UserContext = createContext()

function App() {
	const [user, setUser] = useState()
	const [isLoading, setIsLoading] = useState(true)

	// On mount
	useEffect(() => {
		setIsLoading(false)
		const localStorageUser = localStorage.getItem("user")
		if (localStorageUser === null || localStorageUser === "undefined") return
		const data = JSON.parse(localStorageUser)
		setUser(data)
	}, [])

	// Changes make to user
	useEffect(() => {
		if (user === undefined) return
		localStorage.setItem("user", JSON.stringify(user))
	}, [user])


	return (
		<div className="App">
			<header className="App-header">
				<UserContext.Provider value={{ user, isLoading, setUser }}>
					<BrowserRouter>
						<Navbar />
						<Pages />
					</BrowserRouter>
				</UserContext.Provider>
			</header>
		</div >
	);
}

export default App;
