import React, { useState } from 'react'
import "./InputField.css"

const InputField = ({ textState, submitCallback }) => {
    const [text, setText] = useState("")
    const handleChange = e => {
        setText(e.target.value)
        textState(e.target.value)

    }
    const handleSubmit = e => {
        e.preventDefault()
        submitCallback(e)
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className="input-container">
                <input type="text" className="startup-input" onChange={handleChange} value={text} />
                <button className="startup-confirm"></button>
            </div>
        </form>
    )
}

export default InputField