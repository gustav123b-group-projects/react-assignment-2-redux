import React, { useContext } from 'react'
import { Link } from "react-router-dom";
import { UserContext } from '../../App';
import "./Navbar.css"

const Navbar = () => {
    const { user, setUser } = useContext(UserContext)
    
    // Removing user from local storage when logging out and re-rendering the App.jsx component
    const LogOut = () => {
        localStorage.removeItem("user")
        setUser(undefined)
    }

    return (
        <nav className='v-center'>
            <div className="nav-item logotype">
                <Link to="/">Lost In Translation</Link>
            </div>
            {user !== undefined &&
                <>
                    <div className='nav-item'>
                        <Link to="/translate">Translate</Link>
                    </div>
                    <div className='v-center col-gap-3rem nav-profile-related'>
                        <div className='nav-item'>
                            <Link to="/profile">
                                <div className="v-center col-gap-05em">
                                    <div className="profile-picture"></div>
                                    {user.username}
                                </div>
                            </Link>
                        </div>
                        <div onClick={LogOut} className='nav-item'>
                            <Link to="/">Logout</Link>
                        </div>
                    </div>
                </>
            }
        </nav>
    )
}

export default Navbar