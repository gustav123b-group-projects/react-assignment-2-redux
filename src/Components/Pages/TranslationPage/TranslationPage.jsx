import React, { useState, useContext } from 'react'
import "./TranslationPage.css"
import InputField from '../../InputField/InputField'
import OutputField from '../../OutputField/OutputField'
import { UserContext } from '../../../App';

const TranslationPage = () => {
    const [inputText, setInputText] = useState("")
    const [outputText, setOutputText] = useState(false)
    const { user, setUser } = useContext(UserContext)

    const apiURL = 'https://react-assignment-2-app.herokuapp.com'
    const apiKey = 'CH284750PBwyrckbBjV9dIk5dlVSGrYPsRDU8Zj0egr6Zo8sXewFP1gISF7gVKLn'

    // Transform the input given by the user to sign language images
    const translate = text => {
        const arr = text.split("")
        const images = []
        arr.forEach(arrElement => {
            images.push(arrElement.toLowerCase() + ".png")
        });
        return images
    }

    // Button clicked in input field, this activates the translation
    const btnClicked = async e => {
        // Translate input
        setOutputText(translate(inputText))
        await fetch(`${apiURL}/translations/${user.id}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new translations to add to user with id 1
                translations: [...user.translations, inputText]
            })
        })
        const userCopy = { ...user }
        userCopy.translations.push(inputText)

        setUser(userCopy)
    }

    return (
        <div className="translation-page-container">
            <div className="translate-input-container">
                <div className="page-intro">
                    <h2>Enter the text that you want translated below.</h2>
                    <InputField textState={setInputText} submitCallback={btnClicked} />
                </div>
                <div className="center">
                    <div className="output-container">
                        <OutputField content={outputText} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TranslationPage