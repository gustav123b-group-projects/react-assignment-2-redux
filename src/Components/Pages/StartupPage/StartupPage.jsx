import React, { useState, useContext } from 'react'
import { useNavigate } from "react-router-dom";
import "./StartupPage.css"
import "../Pages.css"
import InputField from '../../InputField/InputField';
import { UserContext } from '../../../App';

const Startup = () => {
    const [name, setName] = useState("")
    const [isInvalidUsername, setIsInvalidUsername] = useState(false)
    const [monster, setMonster] = useState("hide")
    const navigate = useNavigate()
    const { setUser } = useContext(UserContext)

    const apiURL = 'https://react-assignment-2-app.herokuapp.com'
    const apiKey = 'CH284750PBwyrckbBjV9dIk5dlVSGrYPsRDU8Zj0egr6Zo8sXewFP1gISF7gVKLn'
    const usernameMinChar = 3
    const usernameMaxChar = 25

    // Naigate to translate page and re-rendering App.jsx component
    const login = user => {
        navigate("/translate", { replace: true })
        setUser(user)
    }

    const getUser = async () => {
        const url = `${apiURL}/translations?username=${name}`
        const response = await fetch(url)
        return await response.json()
    }

    const createUser = () => {
        if (name.length < usernameMinChar || name.length > usernameMaxChar) {
            setIsInvalidUsername(true)
            return false
        }
        setIsInvalidUsername(false)

        fetch(`${apiURL}/translations`, {
            method: 'POST',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: name,
                translations: []
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not create new user')
                }
                return response.json()
            })
            .then(newUser => {
                login(newUser)
            })
            .catch(error => {
            })
    }

    // Button in log in input field clicked
    const btnClicked = async () => {
        const userData = await getUser()
        if (userData.length === 0) createUser()
        else login(userData[0])
    }

    const revealMonster = () => {
        console.log(monster)
        if (monster === "") setMonster("hide")
        else setMonster("")
    }

    return (
        <>
            <div className="startup-page">

                <div className="page-intro startup-page-intro">
                    <div className='startup-intro-text'>
                        <button onClick={revealMonster}>Reveal the scary monster</button>
                        <div className="h-center">
                            <h1>Translation App</h1>
                            <div className="page-info-text">Please enter your name to log in/create an account</div>
                            <ul>
                                <li>Username must contain {usernameMinChar}-{usernameMaxChar} characters</li>
                            </ul>
                        </div>
                        <div></div>
                    </div>
                    <div className="input-box">
                        <label htmlFor="">Hello, what's your name?</label>
                        <InputField textState={setName} submitCallback={btnClicked} />
                        <div className="border-bottom"></div>
                    </div>
                </div>
                {isInvalidUsername && <div className='invalid-username'>The username must contain {usernameMinChar}-{usernameMaxChar} characters.</div>}
            </div>
            <div className={`monster-container ${monster}`}>
                <div className={`green-monster `}></div>
            </div>
        </>
    )
}

export default Startup