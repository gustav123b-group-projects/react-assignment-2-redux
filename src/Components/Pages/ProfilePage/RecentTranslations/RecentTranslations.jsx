import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { UserContext } from '../../../../App'
import ProfileTranslation from './ProfileTranslation/ProfileTranslation'
import "./RecentTranslations.css"

const RecentTranslations = () => {
    const { user, setUser } = useContext(UserContext)
    const apiURL = 'https://react-assignment-2-app.herokuapp.com'
    const apiKey = 'CH284750PBwyrckbBjV9dIk5dlVSGrYPsRDU8Zj0egr6Zo8sXewFP1gISF7gVKLn'
    const recentPosts = [...user.translations].reverse()
    recentPosts.length = 10

    // Setting translations to an empty array []
    const deleteEntries = async () => {
        await fetch(`${apiURL}/translations/${user.id}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new translations to add to user with id 1
                translations: []
            })
        })
        const userCopy = { ...user }
        userCopy.translations = []
        setUser(userCopy)
    }

    return (
        <div className="recent-translations-box">
            <div className="profile-border-top">Recent translations</div>
            <div className="recent-translations-container">
                <ul>
                    {user !== undefined &&
                        user.translations.length > 0 ?
                        recentPosts.map((translation, i) => {
                            return <ProfileTranslation key={`translation-${i}`} text={translation} />
                        }) : <div>
                            You have no recent translations.. Click
                            <Link to="/translate"> <span className="underline">here</span> </Link>
                            to make some new ones!
                        </div>
                    }
                </ul>
            </div>
            {user !== undefined &&
                user.translations.length > 0 &&
                <div className='center'>
                    <button onClick={deleteEntries} className="profile-delete-btn">Delete all entries</button>
                </div>
            }
        </div>
    )
}

export default RecentTranslations