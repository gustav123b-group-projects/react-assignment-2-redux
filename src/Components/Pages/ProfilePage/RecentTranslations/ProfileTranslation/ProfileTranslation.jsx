import React from 'react'
import "./ProfileTranslation.css"

const ProfileTranslation = ({ text }) => {
    return (
        <li className='translation'>{text}</li>
    )
}

export default ProfileTranslation