import React, { useContext } from 'react'
import { UserContext } from '../../App';
import {
    Routes,
    Route,
} from "react-router-dom";
import StartupPage from './StartupPage/StartupPage'
import TranslationPage from './TranslationPage/TranslationPage';
import ProfilePage from './ProfilePage/ProfilePage';

const Pages = () => {

    const { user } = useContext(UserContext)
    return (
        <Routes>
            <Route path="/" element={<StartupPage />}></Route>

            {/* If the user is not logged in you should not be able to see the translate and profile page. */}
            {user !== undefined &&
                <>
                    <Route path="/translate" element={<TranslationPage />} />
                    <Route path="/profile" element={<ProfilePage />} />
                </>
            }

            {/* If the user tries to go to to translate or profile page without being logged in, the user will be navigated to the start page. */}
            <Route path="*" element={<StartupPage />} />
        </Routes>
    )
}

export default Pages